/*
Game Client
*/
var uname;
var stage;
var layer;
var myPeronalShapes = [];
var socket = io.connect('http://localhost:8000');
var people = [];
var groups = [];
function addPerson(name){
	console.log(name);
	 var group = new Kinetic.Group({
       });
      var box = new Kinetic.Rect({
         x: 100,
        y: 40,
        width: 100, 	
        height: 50,
        fill: '#00D2FF',
        stroke: 'black',
        strokeWidth: 4,
        
      });

      	group.add(box);
       var simpleText = new Kinetic.Text({
         x: box.getX(),
        y: box.getY(),
        text: name,
        fontSize: 30,
        fontFamily: 'Calibri',
        fill: 'green'
      });
       group.add(simpleText);
      // add cursor styling
      group.on('mouseover', function() {
        document.body.style.cursor = 'pointer';
      });
      group.on('mouseout', function() {
        document.body.style.cursor = 'default';
      });
      groups[name] = group;
      simpleText.moveToBottom();
      box.moveToBottom();

      layer.add(group);
}
socket.on('connect', function(){
     uname = prompt("What's your name: ");
     people.push(uname);
    console.log(uname);
    socket.emit('adduser', uname);
    joinGameRoom();
    stage = new Kinetic.Stage({
        container: 'myCanvas',
        width: 578,
        height: 200
      });
    layer = new Kinetic.Layer();
       var group = new Kinetic.Group({
       	draggable: true
       });
      var box = new Kinetic.Rect({
        x: 100,
        y: 40,
        width: 100, 	
        height: 50,
        fill: '#FF69B4',
        stroke: 'black',
        strokeWidth: 4,
        
      });

      	group.add(box);	
      	
       var simpleText = new Kinetic.Text({
        x: box.getX(),
        y: box.getY(),
        text: uname,
        fontSize: 30,
        fontFamily: 'Calibri',
        fill: 'green'
      });
       group.add(simpleText);
      // add cursor styling
      group.on('mouseover', function() {
        document.body.style.cursor = 'pointer';
      });
      group.on('mouseout', function() {
        document.body.style.cursor = 'default';

      });

      group.on('dragend', function() {
      	var data = {};
      	data["x"] = group.getX();
      	data["y"] = group.getY();
      	data["name"] = uname;
      	console.log(JSON.stringify(data));
       socket.emit('messageGameRoom', 'game', 'moveUser', JSON.stringify(data), false);
      });
      layer.add(group);
      stage.add(layer);
});



socket.on('logToChat', function(data){
	console.log(data);
	if($.inArray(data, people) == -1){
		console.log('here');
		people.push(data);
		addPerson(data);
		socket.emit('messageGameRoom', 'game', 'addBlock', uname, true);
	}
		$("#chatArea").append('<p>' + data +  ' joined game room</p>');
		
});

function joinGameRoom(){
	socket.emit('joinGameRoom', 'game', 'logToChat', uname, true);
}

function leaveGameRoom(){
	socket.emit('leaveGameRoom', 'game', 'test', 'left game room');
}

socket.on('test', function(data){
	alert(data);
});

socket.on('recieveMessage', function(id, message){
	if(id == "logToChat")
	{
	
		$("#chatArea").append('<p>' + message + '</p>');
	}	
	else if(id == "addBlock"){
		if($.inArray(message, people) == -1){
		console.log('here');
		people.push(message);
		addPerson(message);
	}
		
	}
	else if(id=="moveUser"){
		console.log(message);
		data = JSON.parse(message);
		//groups[name]
		console.log(groups[data.name]);
        groups[data.name].getChildren().each( function(element){
        	element.setX(data.x);
        	element.setY(data.y);
        });
       layer.draw();

	}
	else{
			alert(message);
	}
});

function messageGameRoom(m){
	socket.emit('messageGameRoom', 'game', 'test', 'hi', m);
}

function CreateRoom(){
	var room = prompt("Room Name: ");
	socket.emit('joinCustomRoom', room, 'test', 'joined custom room ' + room );
}

function messageCreatedRoom(m){
	var room = prompt("Room Name: ");
	var mess = prompt("The message");
	socket.emit('messageCustomRoom', room, 'test', mess, m);
}

function leaveCustomRoom(){
	var room = prompt("Room Name to leave: ");
	socket.emit('leaveCustomRoom', room, 'test', "left custom room " + room, false);
}

function CreateprivateRoom(){
	var room = prompt("Room Name: ");
	socket.emit('joinPrivateRoom', room, 'test', 'joined custom room ' + room );
}

function messagePrivateRoom(m){
	var room = prompt("Room Name: ");
	var mess = prompt("The message");
	socket.emit('messagePrivateRoom', room, 'test', mess, m);
}

function leavePrivateRoom(){
	var room = prompt("Room Name to leave: ");
	socket.emit('leavePrivateRoom', room, 'test', "left custom room " + room, false);
}