# Node JS Chat And Canvas Demo#

This is a node.js chat and canvas demo. 

## Getting it to run ##

Clone this repo into your webroot directory. Such that index.html can be served with your http server. This demo does not have an http server configured to serve the index.html page.

You will need to have [node.js](http://nodejs.org/download/) installed. 

Next you will need to have NPM (the node package manager) installed. Type npm into your command or terminal window to see if its installed.

If it is not installed then follow the instructions [here](http://howtonode.org/introduction-to-npm).

Next you will need to install [socket.io](http://socket.io/docs/) in the directory that the server.js file is in.

You can use the following to install the server:

```
#!bash
npm install socket.io

```

To run the demo type:


```
#!bash

node server.js
```

Then navigate to http://localhost/path-to/index.html
