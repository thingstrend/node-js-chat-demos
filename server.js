// Require dependencies
var clients = [];
var app = require('http').createServer()
, fs = require('fs')
, io = require('socket.io').listen(app);

//http://stackoverflow.com/questions/19156636/node-js-and-socket-io-creating-room
var usernames = [];
var game_rooms = ['game'];//specified at the beggining and cannot be created or destroyed
var created_rooms = [];//can be created and destroyed on the fly
// creating the server ( localhost:8000 )
app.listen(8000);
 io.sockets.on('connection', function(socket) {

  //add a user
  socket.on('adduser', function (username){
        socket.username = username;
        if(typeof usernames[username] == "undefined"){
            usernames[username] = ['connections','customRooms','gameRooms'];
            usernames[username]['connections'] = [];
            usernames[username]['customRooms'] = [];
            usernames[username]['gameRooms'] = [];
        }
            
        socket.privateRooms = [];
        usernames[username]["connections"].push(socket)
        //join other custom rooms that username is in in other socket sessions
        for(var room in usernames[socket.username]['customRooms'])
            socket.join(usernames[socket.username]['customRooms'][room]);
        //join other gameRooms that username is in in other socket sessions
        for(var room in usernames[socket.username]['gameRooms'])
            socket.join(usernames[socket.username]['gameRooms'][room]);
    });

    //join custom room
    socket.on('joinCustomRoom', function(joinRoom, callback, message){
        if(typeof callback == "undefined")
            callback = "";
        //create room if not in existance
        if(created_rooms.indexOf(joinRoom) == -1)
            created_rooms.push(joinRoom);
        if(usernames[socket.username]['customRooms'].indexOf(joinRoom) == -1){//if not already in the room
            //subscribe all the current users socket connections
            usernames[socket.username]['customRooms'].push(joinRoom);//add the room to the list
            //iterate through all socket connections
            for(var user in usernames[socket.username]['connections']){
                usernames[socket.username]['connections'][user].join(joinRoom);
                if(callback != ""){
                    usernames[socket.username]['connections'][user].emit(callback, message);//send message
                }
            }
        }
    });

    //leave a custom room
    socket.on('leaveCustomRoom', function(leaveRoom, callback, message){
        if(typeof callback == "undefined")
            callback = "";
        var pos = created_rooms.indexOf(leaveRoom);
        console.log(pos);
        if(pos != -1){
            console.log('found room');
            //iterate through all socket connections
            for(var user in usernames[socket.username]['connections']){
                usernames[socket.username]['connections'][user].leave(leaveRoom);
                //remove the custom gameroom from the users list
                var pos = usernames[socket.username]['customRooms'].indexOf(leaveRoom);
                usernames[socket.username]['customRooms'].splice(pos,1);
                if(callback != ""){
                    usernames[socket.username]['connections'][user].emit(callback, message);//send message
                }
            }
            var connectionsLeft = io.sockets.clients(leaveRoom);
            if(connectionsLeft < 1){//no one left
                created_rooms.splice(pos, 1);//remove the room
            }
        } 
    });

    //join game room
    socket.on('joinGameRoom', function(joinRoom, callback, message, others){
        if(game_rooms.indexOf(joinRoom) != -1){//make sure the game room exists
            if(typeof callback == "undefined")
                callback = "";
             if(typeof others == "undefined")
                others = false;
            if(usernames[socket.username]['gameRooms'].indexOf(joinRoom) == -1){//if not already in the room
                console.log("joining room");
                //subscribe all the current users socket connections
                usernames[socket.username]['gameRooms'].push(joinRoom);//add the room to the list
                //iterate through all socket connections
                for(var user in usernames[socket.username]['connections']){
                    usernames[socket.username]['connections'][user].join(joinRoom);
                    if(callback != ""){
                        console.log("sending message");
                        if(!others)
                            usernames[socket.username]['connections'][user].emit(callback, message);//send message
                        else{
                            io.sockets.in(joinRoom).emit(callback, message);
                        }
                    }
                }
            }
        }
    });

    //leave a game room
    socket.on('leaveGameRoom', function(leaveRoom, callback, message){
        if(typeof callback == "undefined")
            callback = "";
        if(game_rooms.indexOf(leaveRoom) != -1){
            //iterate through all socket connections
            for(var user in usernames[socket.username]['connections']){
                usernames[socket.username]['connections'][user].leave(leaveRoom);
                //remove gameroom from users list
                var pos = usernames[socket.username]['gameRooms'].indexOf(leaveRoom);
                usernames[socket.username]['gameRooms'].splice(pos,1);
                if(callback != ""){
                    usernames[socket.username]['connections'][user].emit(callback, message);//send message
                }
            } 
        }
    });

    //join private room
    socket.on('joinPrivateRoom', function(joinRoom, callback, message){
        if(typeof callback == "undefined")
            callback = "";
        //create room if not in existance
        if(created_rooms.indexOf(joinRoom) == -1)
            created_rooms.push(joinRoom);
        //only the current socket joins the room
        socket.privateRooms.push(joinRoom);
        socket.join(joinRoom);
        if(callback != ""){
            socket.emit(callback, message);//send message
        }
    });


    //leave private room
    socket.on('leavePrivateRoom', function(leaveRoom, callback, message){
        if(typeof callback == "undefined")
            callback = "";
       var pos = created_rooms.indexOf(leaveRoom);
         if(pos != -1){
            //only current socket leaves
            socket.leave(leaveRoom);
            var privateLoc = socket.privateRooms.indexOf(leaveRoom);
            socket.privateRooms.splice(privateLoc, 1);//remove the private room
            var connectionsLeft = io.sockets.clients(leaveRoom);
            if(connectionsLeft < 1){//no one left
                created_rooms.splice(pos, 1);//remove the room
            }
            if(callback != ""){
                socket.emit(callback, message);//send message
            }
         } 
    });

    socket.on('messageGameRoom', function(gameRoom, message_id, message, self){
        if(game_rooms.indexOf(gameRoom) != -1){
            if(typeof self == "undefined")
            self = true;
            if(self){
                io.sockets.in(gameRoom).emit('recieveMessage',  message_id, message);
            }
            else{
                socket.broadcast.to(gameRoom).emit('recieveMessage',  message_id, message);
            }
        }
    });

    socket.on('messageCustomRoom', function(createdRoom, message_id, message, self){
        if(created_rooms.indexOf(createdRoom) != -1){
            if(typeof self == "undefined")
                self = true;
            if(self){
                io.sockets.in(createdRoom).emit('recieveMessage',  message_id, message);
            }
            else{
                socket.broadcast.to(createdRoom).emit('recieveMessage',  message_id, message);
            }
        }
    });


    socket.on('messagePrivateRoom', function(privateRoom, message_id, message, self){
        if(typeof self == "undefined")
            self = true;
        if(self){
            io.sockets.in(privateRoom).emit('recieveMessage',  message_id, message);
        }
        else{
            socket.broadcast.to(privateRoom).emit('recieveMessage',  message_id, message);
        }
    });


    socket.on('disconnect', function() {
        //remove the current socket
        //disconnect from custom rooms
        if(typeof usernames[socket.username] != "undefined"){
            if(typeof usernames[socket.username]['customRooms'] != "undefined"){
                for(var room in usernames[socket.username]['customRooms']){
                    socket.leave(usernames[socket.username]['customRooms'][room]);
                    //if no one else is left in the room kill it
                    var connectionsLeft = io.sockets.clients(usernames[socket.username]['customRooms'][room]);
                    if(connectionsLeft < 1){//no one left
                        created_rooms.splice(pos, 1);//remove the room
                    }
                }
            }
          
            //disconnect from game rooms
            if(typeof usernames[socket.username]['gameRooms'] != "undefined"){
                for(var room in usernames[socket.username]['gameRooms']){
                    socket.leave(usernames[socket.username]['gameRooms'][room]);
                }
            }

            //disconnect from private socket rooms
            if(typeof socket.privateRooms != "undefined"){
                for(var room in socket.privateRooms){
                    socket.leave(socket.privateRooms[room]);
                    //if no one else is left in the room kill it
                    var connectionsLeft = io.sockets.clients(socket.privateRooms[room]);
                    if(connectionsLeft < 1){//no one left
                        created_rooms.splice(pos, 1);//remove the room
                    }

                }
            }
            //take care of removing the socket from the users list of sockets
            var pos = usernames[socket.username]['connections'].indexOf(socket);
            usernames[socket.username]['connections'].splice(pos,1);//remove the one socket connection from the list
            if(usernames[socket.username]['connections'].length == 0){
                delete usernames[socket.username];
                //notify theat user has left
            }
        }
       
    });
 });